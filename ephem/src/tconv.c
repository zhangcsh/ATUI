#include <stdio.h>
#include <math.h>
#include "ephem.h"

int main(int argc, char *argv[])
{
  double mjdtai,mjd,jd,dt,t0,newtime,sstai,sstdt,ssutc;
  double dayfrac,utc,tdt,tai;
  int hhtai,hhtdt,hhutc,mmtai,mmtdt,mmutc;

  #ifdef __MACOS__
    #include <console.h>
    argc = ccommand(&argv);
  #endif

  sscanf(argv[1],"%lf",&mjdtai);

/*  printf("MJD = %lf\n",mjdtai);	*/

  jd = mjdtai / 86400.0 + 2400000.5;
/*  dt = dyntime(jd);			*/

  dayfrac = jd - floor(jd) + 0.5;
/*  printf("day fraction is: %lf\n",dayfrac);	*/
  tai = 24.0 * fmod(dayfrac,1.000);
/*  printf("tai in dec hours is: %lf\n",tai);	*/
  tdt = tai + TDT_TAI / 3600.0;
  utc = tai - TAI_UTC / 3600.0;

  hms(tai,&hhtai,&mmtai,&sstai);
  hms(tdt,&hhtdt,&mmtdt,&sstdt);
  hms(utc,&hhutc,&mmutc,&ssutc);

  printf("TAI = %02u:%02u:%09.6lf\n",hhtai,mmtai,sstai);
  printf("TDT = %02u:%02u:%09.6lf\n",hhtdt,mmtdt,sstdt);
  printf("UTC = %02u:%02u:%09.6lf\n",hhutc,mmutc,ssutc);

  return 0;
}
