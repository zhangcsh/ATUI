#include <stdio.h>
#include <math.h>
#include "ephem.h"

int main(int argc, char *argv[])
{

  double mjd,mjdtai,jde,jdc,jd1,tdt,utc,ut1,dt,tmp,ss,ss2,dut1;
  double alpha,delta,range,stationlat,stationelev;
  double psi,eps,obliq,st;
  double topodelt,topora,hourang,azimuth,elev,temp,press,ref;
  int hh,mm,hh2,mm2;
  long int mjdin;
  char str[80],s1[10],s2[10],s3[10],s4[10],s5[10],s6[10],s7[10];
  char *path=DATADIR;
  FILE *fp;

  if (argc < 5) {
    printf("usage: geo2azel ra.deg dec.deg mjdtai.sec range.au\n");
    return 1;
  }
  sscanf(argv[1],"%lf",&alpha);
  sscanf(argv[2],"%lf",&delta);
  sscanf(argv[3],"%lf",&mjdtai);
  sscanf(argv[4],"%lf",&range);
  range *= AUTOKM;

  tdt = mjdtai + TDT_TAI;
  mjd = mjdtai / 86400.0;
  utc = mjdtai - TAI_UTC;
  jdc = utc/86400.0 + 2400000.5;

  dt = dyntime(jdc);

  if (mjd > 52058.5 && mjd < 52423.5) {
    fp = myopen(path,"dut1.dat","r");
    fgets(str,80,fp);
    while (strstr(str,"END") == NULL) {
      sscanf(str,"%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7);
      sscanf(s4,"%ld",&mjdin);
      sscanf(s7,"%lf",&tmp);
      if (fabs(mjd - (double) mjdin) < 0.5) {
        dut1 = tmp;
      }
      fgets(str,80,fp);
    }
    fclose(fp);
    dt = TDT_TAI + TAI_UTC - dut1;
  }
  printf("DT = %lf\n",dt);
  ut1 = tdt - dt;

  jde = tdt/86400.0 + 2400000.5;
  jd1 = ut1/86400.0 + 2400000.5;

  printf("I show UTC - UT1 = %lf\n",utc-ut1);
  printf("JDC = %lf\n",jdc);
  printf("JD1 = %lf\n",jd1);

  nutation(jde,&psi,&eps,&obliq);

  st = siderial(jdc) + APOLONG;
  st += psi*cos((obliq + eps/3600.0)*DTR)/3600.0;
  st -= 360.0*floor(st/360.0);
  tmp = st/15.0;
  hms(tmp,&hh,&mm,&ss);
  printf("\nApparent Siderial time is: %12.8lf = %02u:%02u:%07.4lf\n",
           st,hh,mm,ss);

  stationlat = APOLAT;
  stationelev = APOELEV;
  topo(st,alpha,delta,range,stationlat,stationelev,&topodelt,&topora);

  printf("Correction to RA, dec is: %lf, %lf\n",topora-alpha,topodelt-delta);

  hourang = st - topora;
  azel(hourang,topodelt,&azimuth,&elev);

  hms(azimuth,&hh,&mm,&ss);
  hms(elev,&hh2,&mm2,&ss2);
  printf("Azimuth, elevation: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n
",
          azimuth,elev,hh,mm,ss,hh2,mm2,ss2);

/* Correct for atmospheric refraction                           */

  temp = 283.0;
  press = 717.0;
  ref = refraction(elev,temp,press);
  tmp = elev + ref;
  hms(tmp,&hh,&mm,&ss);
  if (fabs(ref) > 0.0) {
    printf("Elev = %lf = %d:%02u:%04.1lf, refracted by %lf arcsec\n",
            tmp,hh,mm,ss,ref*3600.0);
  }

  st = siderial(jd1) + APOLONG;
  st += psi*cos((obliq + eps/3600.0)*DTR)/3600.0;
  st -= 360.0*floor(st/360.0);
  tmp = st/15.0;
  hms(tmp,&hh,&mm,&ss);
  printf("\nApparent Siderial time is: %12.8lf = %02u:%02u:%07.4lf\n",
           st,hh,mm,ss);

  stationlat = APOLAT;
  stationelev = APOELEV;
  topo(st,alpha,delta,range,stationlat,stationelev,&topodelt,&topora);

  hourang = st - topora;
  azel(hourang,topodelt,&azimuth,&elev);

  hms(azimuth,&hh,&mm,&ss);
  hms(elev,&hh2,&mm2,&ss2);
  printf("Azimuth, elevation: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n
",
          azimuth,elev,hh,mm,ss,hh2,mm2,ss2);

/* Correct for atmospheric refraction                           */

  temp = 283.0;
  press = 717.0;
  ref = refraction(elev,temp,press);
  tmp = elev + ref;
  hms(tmp,&hh,&mm,&ss);
  if (fabs(ref) > 0.0) {
    printf("Elev = %lf = %d:%02u:%04.1lf, refracted by %lf arcsec\n",
            tmp,hh,mm,ss,ref*3600.0);
  }

  return 0;
}
