#include <math.h>
#include "ephem.h"

/* This routine performs the 3D vector arithmetic to compute the	*/
/* apparent geocentric position of a feature on the moon.  Passed in	*/
/* are pointers to: selenographic longitude and latitude (in degrees),	*/
/* the 9-element rotation matrix including libration, position angle,	*/
/* and lunar position, and the 3D XYZ vector to the moon in km (X ->	*/
/* RA=DEC=0).  The returned values are the declination, right ascension	*/
/* (both deg.), and range in km (all geocentric) of the lunar feature.	*/

void moonfeat(double slong, double slat, double totrot[], double rmoon[], double *decfeat, double *rafeat, double *rangefeat)
{

  double cl,sl,cb,sb,x,y,z,mrad,xmoon[3],rfeat[3];

  cl = cos(slong * DTR);
  sl = sin(slong * DTR);
  cb = cos(slat * DTR);
  sb = sin(slat * DTR);

  mrad = 1737.4;
  xmoon[0] = mrad*cl*cb;  xmoon[1] = mrad*sl*cb;  xmoon[2] = mrad*sb;

  mv3x3(totrot,xmoon,rfeat);

  x = rmoon[0] + rfeat[0];
  y = rmoon[1] + rfeat[1];
  z = rmoon[2] + rfeat[2];

  *rangefeat = sqrt(x*x + y*y + z*z);

  *decfeat = RTD * asin(z / *rangefeat);
  *rafeat = RTD * atan2(y,x);
  *rafeat -= 360.0*floor(*rafeat/360.0);

}
