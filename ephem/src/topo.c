#include <stdio.h>
#include <math.h>
#include "ephem.h"

/* Converts geocentric apparent coordinates (RA & dec) and geocentric	*/
/* range in km into topocentric apparent coordinates.  The latitude is	*/
/* expressed in degrees, the elevation is in meters above MSL. The 	*/
/* apparent siderial time (in deg) is also passed as the first argument.*/


void topo(double st, double alpha, double delta, double range, double latitude, double elevation, double *topodelt, double *topora)
{
  double flat,x,rhost,rhoct,paral,hourang,da,numer,denom;

  flat = 0.99664719;
  x = flat * tan(latitude * DTR);
  rhost = flat * x/sqrt(1.0 + x*x) + elevation*sin(latitude * DTR)/6378136.0;
  rhoct = 1.0/sqrt(1.0 + x*x) + elevation*cos(latitude * DTR)/6378136.0;
  printf("rho = %f\n",sqrt(rhost*rhost + rhoct*rhoct)*6378136.0);

  paral = 6378.136 / range;
  hourang = st - alpha;
  denom = cos(delta*DTR) - rhoct*paral*cos(hourang*DTR);
  da = atan(-rhoct*paral*sin(hourang*DTR)/denom);
  numer = (sin(delta*DTR) - rhost*paral)*cos(da);
  *topodelt = atan(numer/denom) * RTD;
  *topora = alpha + da * RTD;
  *topora -= 360.0*floor(*topora / 360.0);

}
